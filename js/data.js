var listEmployee =[
{
    id: 1,
    name: 'Ariel',
    surname: 'Mergen',
    sector:'Desarrollo'
    
},
{
	id: 2,
    name: 'Roque',
    surname: 'Ferreira',
    sector:'Desarrollo'
},
{
	id: 3,
    name: 'Ivan',
    surname: 'Arias',
    sector:'Dti'
},{
	id: 4,
    name: 'Agustina',
    surname: 'Ruggero',
    sector:'Dti'
},{
	id: 5,
    name: 'Everton',
    surname: 'Barroso',
    sector:'Dti'
},{
	id: 6,
    name: 'Maximiliano',
    surname: 'Barrionuevo',
    sector:'Dti'
},{
	id: 7,
    name: 'Shirley',
    surname: 'Tuller',
    sector:'AOP'
},{
	id: 8,
    name: 'Gisela',
    surname: 'Lopez',
    sector:'AOP'
},{
	id: 9 ,
    name: 'Carolina',
    surname: 'Coutinho',
    sector:'AOP'
}];

function camposabuscar(u){
	return u.name+u.surname+u.sector;
}
var listEmployeeMapped = listEmployee.map(function(u){
	return {listEmployee:u,fields:camposabuscar(u).toUpperCase()}
	//recorre y rearma el array	
})

function myFunction(){
	var inputField = $("#myInput").val();
	var filtrado = listEmployeeMapped.filter(function(u2){
		return u2.fields.indexOf(inputField.toUpperCase())!=-1;
	}).map(function(u3){
		return u3.listEmployee
	})
   prepareHtml(filtrado);
}
function prepareHtml(arr)
{
	var listEmployee = new Array();
	var AllSector = new Array();
	var listSector = new Array();
	var employee = new Array();
	$.each(arr,function(key,value){
		AllSector.push(value.sector);
		listEmployee.push(value);
	});
	var noDuplicateListSector = unique(AllSector);
	$.each(noDuplicateListSector,function(key,value){
		listSector.push({"sector":value});
	});
	html(listSector,listEmployee);
}
function html(sectors,employees){
	$("#result").empty();
	var element="";
	$.each(sectors,function(a,b){
		element += '<div class="panel panel-primary">';
		element += '<div class="panel-heading">';
			if (b.sector!=null) {
				element += '<h3 class="panel-title">'+b.sector+'</h3>';	
			}
		element +='</div>'
		element += '<div class="panel-body">'
		$.each(employees,function(c,d){
				if (b.sector == d.sector) {
					 element += '<p>'+d.name+'</p>';
					 element += '<p>'+d.surname+'</p>';
				}
		});
		element += '</div>';
		element += '</div>';
	});
	$("#result").append(element);
}
function unique(array){
    return array.filter(function(el,index,arr){
        return index == arr.indexOf(el);
    });
}